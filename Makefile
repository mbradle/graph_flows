#///////////////////////////////////////////////////////////////////////////////
#  Copyright (c) 2017 Clemson University.
# 
#  This file was originally written by Bradley S. Meyer.
# 
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this software; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#  USA
# 
#/////////////////////////////////////////////////////////////////////////////*/

#///////////////////////////////////////////////////////////////////////////////
#//!
#//! \file Makefile
#//! \brief A makefile to generate code for the graph project.
#//!
#///////////////////////////////////////////////////////////////////////////////

ifndef NUCNET_TARGET
NUCNET_TARGET = ../nucnet-tools-code
endif

ifndef WN_USER_TARGET
WN_USER_TARGET = ../wn_user
endif

#///////////////////////////////////////////////////////////////////////////////
# Here are lines to be edited, if desired.
#///////////////////////////////////////////////////////////////////////////////

SVNURL = http://svn.code.sf.net/p/nucnet-tools/code/trunk

BUILD_DIR = $(WN_USER_TARGET)/build
VENDORDIR = $(BUILD_DIR)/vendor
OBJDIR = $(BUILD_DIR)/obj

NNT_DIR = $(NUCNET_TARGET)/nnt
USER_DIR = $(NUCNET_TARGET)/user

H5 = h5c++

FC = gfortran

#///////////////////////////////////////////////////////////////////////////////
# End of lines to be edited.
#///////////////////////////////////////////////////////////////////////////////

#===============================================================================
# Svn.
#===============================================================================

SVN_CHECKOUT := $(shell if [ ! -d $(NUCNET_TARGET) ]; then svn co $(SVNURL) $(NUCNET_TARGET); else svn update $(NUCNET_TARGET); fi )

#===============================================================================
# Includes.
#===============================================================================

include $(BUILD_DIR)/Makefile
include $(USER_DIR)/Makefile.inc

VPATH = $(BUILD_DIR):$(NNT_DIR):$(USER_DIR):$(WN_USER_TARGET)

H5 = h5c++

#===============================================================================
# Add local directory to includes.
#===============================================================================

CFLAGS += -I ./ -I $(WN_USER_TARGET)

#===============================================================================
# Debugging, if desired.
#===============================================================================

ifdef DEBUG
  CFLAGS += -DDEBUG
  FFLAGS += -DDEBUG
endif

#===============================================================================
# my_user.
#===============================================================================

ifdef WN_USER
  CFLAGS += -DWN_USER
endif

ifdef HYDRO_EVOLVE
  CFLAGS += -DHYDRO_EVOLVE
endif

#===============================================================================
# Objects.
#===============================================================================

GRAPH_OBJ = $(WN_OBJ)	\
               $(WN_USER_OBJ)	\
               $(NNT_OBJ)	\
               $(USER_OBJ)	\
               $(OPTIONS_OBJ)	\

GRAPH_DEP = $(GRAPH_OBJ)

CLIBS += -lgfortran

#===============================================================================
# Executables.
#===============================================================================

GRAPH_EXEC = graph_flows

$(GRAPH_EXEC): $(GRAPH_DEP)
	$(HC) -c -o $(OBJDIR)/$@.o $@.cpp
	$(HC) $(GRAPH_OBJ) -o $(BINDIR)/$@ $(OBJDIR)/$@.o $(FLIBS) $(CLIBS)

.PHONY all_graph_flows: $(GRAPH_EXEC)

#===============================================================================
# Clean up. 
#===============================================================================

.PHONY: clean_graph_flows cleanall_graph_flows

clean_graph:
	rm -f $(GRAPH_OBJ)

cleanall_graph_flows: clean_graph_flows
	rm -f $(BINDIR)/$(GRAPH_EXEC) $(BINDIR)/$(GRAPH_EXEC).exe

clean_nucnet:
	rm -fr $(NUCNET_TARGET)
