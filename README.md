# graph_flows #

This project creates chart of the nuclide diagrams that show reaction flows
in network calculations.

### Steps to install ###

First, clone the repository by typing 

**git clone https://bitbucket.org/mbradle/graph_flows.git**

Then ensure that wn_user is installed.  If you have not alreadly done so, type

**git clone https://bitbucket.org/mbradle/wn_user.git**

Now change into the *graph_flows* directory and create the project by typing

**cd graph_flows**

**./project_make**

## Steps to create the diagrams ##

First retrieve an example xml file by typing

**curl -o example.xml -J -L https://osf.io/xguz2/download**

Next create a directory, preferably called *output*, where the output [graphviz](https://graphviz.org) files will be stored:

**mkdir output**

Run the *graph_flows* code on a previously created *single_zone_network* output xml file.  As an initial example, use the example input file that you retrieved over the web.  Thus, type

**./graph_flows --libnucnet_xml example.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 3 and z <= 10 and a - z >= 4 and a - z <= 12]"**

This example execution will read the data *example.xml* file and
create the graph flow diagrams from those data.  It will create a subset of the full network
with atomic number *Z* in the range 3 <= Z <= 10 and neutron number *N = A - Z*, where *A* is the mass number,
also in the range 4 <= N <= 12.
The output from the execution will be a number
[graphviz](https://graphviz.org) files (labeled *out_x.dot*, where *x* is the
sequential label) in the directory *output*.  To run on your own *single_zone_network* output xml file called, say,
*my_out.xml*, first clean up the output directory by typing

**rm output/\***

Then execute the code on your file by typing

**./graph_flows --libnucnet_xml my_out.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 3 and z <= 10 and a - z >= 4 and a - z <= 12]"**

Use other options, as desired.  To see the other options available in running the *graph_flows* code, type

**./graph_flows --help**

and

**./graph_flows --prog all**

## Steps to create pdf files ##

Once you have created the appropriate *dot* files, you can convert them into pdfs.  To do so, follow these [steps](https://osf.io/yc84b/wiki/home/).

## Steps to animate the diagrams ##

It is possible to combine the pdf files previously created into an animation.
To do so, follow these [steps](https://osf.io/792fy/wiki/home/)
